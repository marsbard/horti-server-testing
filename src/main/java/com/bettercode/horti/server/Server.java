package com.bettercode.horti.server;

import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Server {


	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(Server.class);
	private static ClassPathXmlApplicationContext context;

	private ActiveMQComponent activemq;
	
	
	public static void main(String[] args) throws Exception {
		context = new ClassPathXmlApplicationContext("/META-INF/spring/context.xml");
		Thread.sleep(500); // no idea why, but without this it just stops without starting the camel server!! >.< TODO TODO

	}


	public void setActivemq(ActiveMQComponent activemq) {
		this.activemq = activemq;
	}
}
