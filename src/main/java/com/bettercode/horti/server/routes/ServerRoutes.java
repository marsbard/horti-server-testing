package com.bettercode.horti.server.routes;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.log4j.Logger;

public class ServerRoutes extends RouteBuilder {

	final private static Logger log = Logger.getLogger(ServerRoutes.class);

	@Override
	public void configure() throws Exception {
		
		
		log.info("configuring server routes");
		
		from("activemq:queue:telemetry").process(new Processor() {

			public void process(Exchange exchange) throws Exception {
				
				log.info(exchange.getIn().getBody());
			}

		}).to("mock:graphite");
		

	}

}
